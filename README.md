# InstaBreShop API

This repo contains the crawler for the BuyCho app, which fill our database with info from instagram.

Currently, the images are stored in a GCP Storage instance, and the file info in a GCP Firestore info.

## Setting up the project

### Setting up GCP Authentication

First, you need to generate a credential file on Google Cloud Platform:

1. Create a project, if you don't already have one
2. Go to **API & Services** -> **Credentials**
3. Click on **Create credentials** -> **Service account key**
4. Select **New service account**, fill the required field, and click on create
5. This will download a JSON file with your credentials, save it on your computer
6. export the path to the file as GOOGLE_APPLICATION_CREDENTIALS, or add it to .env file

- Alternatively, if your project exists, and already have a service account for developers, you can go to:
- **IAM & admin** -> **Service accounts**, and create a key to the account, which will download the JSON file

### Docker

For development, you can use Docker, so you dont have to install the dependencies.

To use, run **make install** then **make build** in a terminal, to start the typescript watcher,
and in another terminal run **make dev** to run the serve on watch mode also.
