require('dotenv').config()

import * as cors from 'cors'
import * as express from 'express'
import * as http from 'http'

import User from './model/user'

const app = express()
app.use(cors({ origin: true }))

app.get('/get_user', async (req, res) => {
  const query = req.query
  console.log();
  
  const brechoName = query.user
  console.log(`==> ${brechoName}`)
  const brecho = new User(brechoName)

  await brecho.loadUserInfo()
  await brecho.addPosts(brecho.numPosts)
  await brecho.downloadPostImages()

  return res.send('Images Downloaded')
})

app.get('/update_db', async (req, res) => {
  const brechosList: Array<string> = [
    'brechozineo_',
    'brechodani_slz',
    // 'brechoit_',
    // 'brechoautenticslz',
    // 'xoxo_brecho',
    // 'brechomariajoaquina',
    // 'brecho.causaanimal',
    // 'brechogarimposlz',
    // 'brechopassarinho_',
  ]

  for (const brechoName of brechosList) {
    http.get(`http://localhost:3000/get_user?user=${brechoName}`)
  }

  return res.send('done')
})

const port: number = 3000

app.listen(port, () => {
  console.log(`Server Started on port ${port}`)
})

export default app
