import * as request from 'request'
import * as cheerio from 'cheerio'
import Post from './post'

export default class User {
  name: string
  url: string
  biography: string = ''
  externalUrl: string = ''
  following: number = 0
  followedBy: number = 0
  profilePic: string = ''
  fullName: string = ''
  endCursor: string = ''
  id: string = ''
  numPosts: number = 0
  posts: Array<Post> = []
  firstPageInfo: any = {}
  firstPageEdges: any = {}

  constructor(name: string) {
    this.name = name
    this.url = `https://www.instagram.com/${name}`
  }

  async loadUserInfo() {
    const userInfo = await this.getUserInfos()

    this.biography = userInfo.biography
    this.externalUrl = userInfo.external_url
    this.following = userInfo.edge_follow.count
    this.followedBy = userInfo.edge_followed_by.count
    this.profilePic = userInfo.profile_pic_url_hd
    this.fullName = userInfo.full_name
    this.id = userInfo.id
    this.firstPageInfo = userInfo.page_info
    this.numPosts = userInfo.count
    this.firstPageEdges = userInfo.edges
  }

  private getUserInfos(): Promise<any> {
    return new Promise((resolve, reject) => {
      request(this.url, (err, _, html: string) => {
        if (err) reject(err)

        const $ = cheerio.load(html)
        const script = $('script')
          .eq(4)
          .html()
        if (!script) reject('Error getting instagram script')

        try {
          const info = this.parseScript(script)
          resolve(info)
        } catch (err) {
          reject(err)
        }
      })
    })
  }

  /**
   * Execute graphql script and get the response
   * @param script Script to execute to get the graphql response
   */
  private parseScript(script: string | null) {
    if (!script) return null

    const parsed = JSON.parse(/window\._sharedData = (.+);/g.exec(script)![1])
    if (!parsed) return null

    const {
      entry_data: {
        ProfilePage: [
          {
            graphql: {
              user: {
                biography,
                external_url,
                edge_followed_by,
                edge_follow,
                full_name,
                profile_pic_url_hd,
                id,
                edge_owner_to_timeline_media: { count, page_info, edges },
              },
            },
          },
        ],
      },
    } = parsed

    return {
      biography,
      external_url,
      edge_followed_by,
      edge_follow,
      full_name,
      profile_pic_url_hd,
      id,
      page_info,
      edges,
      count,
    }
  }

  async addPosts(nPosts: number) {
    console.log('Adding Posts')

    // Add first page posts
    for (const edge of this.firstPageEdges) {
      const newPost: Post = new Post(edge)
      this.posts.push(newPost)
    }
    if (nPosts > 12) await this.getNextPosts(nPosts - 12)
  }

  private getNextPosts(numImages: number) {
    console.log('Getting Next Posts')

    const queryHash = 'f2405b236d85e8296cf30347c9f08c2a'
    const endCursor = this.firstPageInfo.end_cursor

    const url = `https://www.instagram.com/graphql/query/?query_hash=${queryHash}&variables=%7B%22id%22%3A%22${this.id}%22%2C%22first%22%3A${numImages}%2C%22after%22%3A%22${endCursor}%22%7D`

    return new Promise((resolve, reject) => {
      request(url, async (err, _, html) => {
        if (err) reject(err)

        const data = JSON.parse(html)

        const {
          data: {
            user: {
              edge_owner_to_timeline_media: { edges },
            },
          },
        } = data

        for (const edge of edges) {
          const newPost = new Post(edge)
          this.posts.push(newPost)
        }
        resolve(true)
      })
    })
  }

  async downloadPostImages() {
    console.log('Downloading Images')

    for (const post of this.posts) {
      try {
        await post.saveMedia()
        // await post.saveToStorageLocal()
      } catch (err) {
        throw new Error(err.message)
      }
    }
  }
}
