import * as fs from 'fs'
import * as request from 'request'
import { Storage } from '@google-cloud/storage'

const storage = new Storage()
const bucket = storage.bucket('buycho-data-1')

export default class Post {
  typename: string
  id: string
  shortcode: string
  displayUrl: string
  likedBy: number
  isVideo: boolean
  user: string

  destination: string

  constructor(info: any) {
    const node = info.node

    this.typename = node.__typename
    this.id = node.id
    this.shortcode = node.shortcode
    this.displayUrl = node.display_url
    this.likedBy = node.edge_media_preview_like.count
    this.isVideo = node.is_video
    this.user = node.owner.username
    this.destination = `/tmp/${this.user}/${this.shortcode}.jpg`
  }

  private resolveDownloadToStorage() {
    return new Promise((resolve, reject) => {
      // const file = fs.createWriteStream(this.destination)
      const name: string = `${Date.now()}_${this.shortcode}.jpg`
      const fileObj = bucket.file(`${this.user}/${name}`)

      const file = fileObj.createWriteStream({
        metadata: {},
      })
      const getRequest = request.get(this.displayUrl)

      getRequest.on('response', response => {
        if (response.statusCode !== 200)
          reject(`Invalid response status code: ${response.statusCode}`)

        getRequest.pipe(file)

        file.on('finish', () => {
          file.end()
          resolve()
        })
      })

      getRequest.on('error', err => {
        fs.unlink(this.destination, err2 => {
          console.log('Unliked: ', err)
        })
        reject(err.message)
      })
    })
  }

  async saveMedia() {
    try {
      const result = await this.resolveDownloadToStorage()
      return result
    } catch (err) {
      throw new Error(err.message)
    }
  }

  // async saveToStorageStream() {
  //   const name: string = `${Date.now()}_${this.shortcode}.jpg`
  //   const file = bucket.file(`${this.user}/${name}`)

  //   const stream = file.createWriteStream({
  //     metadata: {},
  //   })
  // }

  async saveToStorageLocal() {
    return new Promise(async (resolve, reject) => {
      const name: string = `${Date.now()}_${this.shortcode}.jpg`

      const file = bucket.file(`${this.user}/${name}`)

      try {
        await bucket.upload(this.destination, {
          destination: `${this.user}/${name}`,
          resumable: true,
          metadata: {
            metadata: {
              event: 'lkasdfjlkasdjf',
            },
          },
        })
        await file.makePublic()
        resolve()
      } catch (err) {
        reject(err)
      }
    })
  }

}
