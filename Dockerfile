FROM node:lts

# Set up app
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY . .
RUN npm run build
EXPOSE 3000

# Directory to store downloaded images temporaly
RUN mkdir -p /tmp
VOLUME /tmp

CMD ["npm", "start"]